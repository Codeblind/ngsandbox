'use strict';
var gulp = require('gulp');
var webserver = require('gulp-webserver');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var ngHtml2Js = require('gulp-ng-html2js');
var components = [
  'node_modules/angular/angular.js'
];

gulp.task('serve', function() {
  gulp.src(['./src'])
    .pipe(webserver({
      port: 8081,
      livereload: false,
      directoryListing: false,
      https: true,
      open: true
    }));
});

gulp.task('scripts', function() {
  return gulp.src('src/app/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(sourcemaps.write())
    .pipe(uglify())
    .pipe(gulp.dest('src/assets/js'));
});

gulp.task('html2js', function() {
  return gulp.src('./src/app/directives/**/*.html')
    .pipe(ngHtml2Js({
      moduleName: 'ngSandbox',
      prefix: '/app/directives/'
    }))
    .pipe(gulp.dest('src/assets/js/directives'))
    .pipe(concat('directives.js'))
    .pipe(uglify())
    .pipe(gulp.dest('src/assets/js'));
});

gulp.task('components', function() {
  return gulp.src(components)
    .pipe(sourcemaps.init())
    .pipe(concat('components.js'))
    .pipe(sourcemaps.write())
    .pipe(uglify())
    .pipe(gulp.dest('src/assets/js'));
});

gulp.task('styles', function() {
  return gulp.src(['src/sass/styles.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass()).on('error', sass.logError)
    .pipe(autoprefixer({ browsers: ['last 3 versions', 'IE 9', 'IE 10'] }))
    .pipe(concat('styles.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('src/assets/css'))
    .pipe(minifycss())
    .pipe(gulp.dest('build/assets/css'));
});

gulp.task('build', ['scripts', 'components', 'html2js', 'styles']);

gulp.task('default', ['serve'], function() {
  gulp.watch('src/sass/**/*.scss', ['styles']);
  gulp.watch('node_modules/**/*.js', ['components']);
  gulp.watch('src/app/**/*.js', ['scripts']);
  gulp.watch('src/app/**/*.html', ['styles']);
});

