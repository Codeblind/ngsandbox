angular.module('ngSandbox', [])
  .controller('selectListController', ['$scope', function($scope) {
    $scope.data = {
      model: null,
      availableOptions: [
        { id: '1', name: 'Option A' },
        { id: '2', name: 'Option B' },
        { id: '3', name: 'Option C' },
        { id: '4', name: 'Option D' },
        { id: '5', name: 'Option E' }
      ],
      selectedOption: { id: '4' }
    };
  }]).directive('selectList', function() {
    return {
      templateUrl: '/app/directives/selectList.html'
    }
});
